<?php


class UserMeta extends Connection{

    public function __construct() {
		parent::__construct();
		}
	public function add( $user_id ,$meta_key ,$meta_value , $remote_id = null ){
		$query = $this->db->insert("user_meta",
		[
		"meta_id" => $user_id,
		"meta_key" => $meta_key,
		"meta_value" => $meta_value,
		"remote_id" => $remote_id
		]);
		return $this->db->id() > 0 ? true  : false;
		}
	public function update($id, $field , $new_value ){
		$id_new =  ( int )$id;
		$update = $this->db->update("user_meta",
										[
										$field => $new_value
										],
										[
										"id" => $id_new
										]);
										
			return $update > 0 ? true : false;
		}
	public function delete($id){
		$id_new =  ( int )$id;
		$query = $this->db->delete("user_meta",
									[
									"id" => $id_new,
									"LIMIT" => 1
									]);
									
		return $query > 0 ? true : false;
		
		}
   public function get($identifier, $pagnate = [] ){
	   
	   $limit = is_int($pagnate[0]) && is_int($pagnate[1]) ?
		[
            ( ( int ) $pagnate[0] - 1 ) * ( int ) $pagnate[1],
            ( int ) $pagnate[1]
        ] :[0,1];
	   
	   $columns = is_numeric($identifier) ? "id" : "meta_key";
	   $records = $this->db->select("user_meta",[
											   "meta_id",
											   "meta_key",
											   "meta_value"
											   ],[
											   $columns => $identifier,
											   "LIMIT" => $limit
											   ]);
											   return !empty($records) ? $records : false;
	   }

}
// should be  the same as the class name

?>
