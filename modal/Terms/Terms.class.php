<?php


class Terms extends Connection {

    


    public function __construct() {
		parent::__construct();
		}
		
	public function add($name,$taxonomy,$parent_id = null, $remote_id = null){
		$this->db->insert("terms",
									[
									"name" => $name,
									"taxonomy" => $taxonomy,
									"parent_id" => $parent_id,
									"remote_id" => $remote_id
									]);
			return $this->db->id() > 0 ? $this->db->id() : false;
		}
	public function get($identifier, $pagnate = []){
		$limit = is_int($pagnate[0]) && is_int($pagnate[1]) ?
		[
            ( ( int ) $pagnate[0] - 1 ) * ( int ) $pagnate[1],
            ( int ) $pagnate[1]
        ] :[0,1];
		
		$column = is_numeric( $identifier ) ? 'id' : 'name';
		$records = $this->db->select("terms",
											[
											"name",
											"taxonomy",
											"parent_id",
											"remote_id"
											],
											[
											$column => $identifier,
											"LIMIT" => $limit
											]);
			return !empty($records) ? $records : [];
	}
	
	public function update($id,$field,$new_value){
		$new_id = (int) $id ;
		$query = $this->db->update("terms",
											[
											$field => $new_value
											],[
											"id" => $new_id
											]);
					return $query > 0 ? $query." updated" : "Failed";
		}
	public function delete($identifier){
		$column = is_numeric($identifier) ? "id" : "name";
		$query = $this->db->delete("terms",
									[
									$column => $identifier
									]);
		return $query > 0 ? true : false;
		}
}
// should be  the same as the class name

?>
