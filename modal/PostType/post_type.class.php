<?php
//require_once('/../app.php');
//require_once ('Medoo.php');

class PostTypes  extends Connection{

   


    public function __construct() {
		parent::__construct();
		
	}
	public  function add($singular,$prural,$remote_id = null){
		$id=( int )$remote_id;
		$this->db->insert("post_types", [
										"singular" => $singular,
										"plural" => $prural,
										"remote_id" => $id
									]);
		return  $this->db->id() == 0 ? false : $this->db->id()   ;
	}

	public function get($param = null){
		if($param != null){
			$columns = ["singular","plural"];
			$col = is_int($param)? "id" : $columns;
			$records = $this->db->select("post_types", [
											$col
												], []);

			return !empty($records) ? $records[0] : [];

		}
		else{

			$records = $this->db->select("post_types", "*");

			return !empty($records) ? $records : [];
		}
	}

	public function update($id,$field,$new_value){
		$id = ( int )$id;
		$query=$this->db->update("post_types", [
										$field => $new_value
										],
										["id" => $id
										]);
		return $query >0 ? true : false;
	}
	
   public function delete($id){
		$id = (int)$id;
		$query = $this->db->delete("post_types", [
											"id" => $id
											]);
		return $query > 0 ? true : false;
   }


}
// should be  the same as the class name

?>
